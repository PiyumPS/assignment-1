#include<stdio.h>
#include<string.h>

struct students{
	char name[20];
	char subject[10];
	float marks;

};

int main()
{
	struct students d[5];
	int i,j,k;
	for(i=0;i<5;i++)
	{
		printf("Enter the %d student's name,subject, and marks: \n",i+1);
		printf("Enter the first name:");
		scanf("%s",&d[i].name);
		printf("Enter the subject:");
		scanf("%s",&d[i].subject);
		printf("Enter the marks:");
		scanf("%f",&d[i].marks);
		printf("---------------------------------------\n");
	}
	
	for(j=0;j<5;j++)
	{
		printf("Details of the %dth student\n",j+1);
		printf("%s\n",d[j].name);
		printf("%s\n",d[j].subject);
		printf("%.2f\n",d[j].marks);
		printf("---------------------------------------\n");
	}
	return 0;
}
