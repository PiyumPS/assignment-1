#include<stdio.h>
int pattern(int);
int make(int,int);

int pattern(int x)
{
	if(x>0)
	{
		printf("%d",x);
		pattern(x-1);
	}
}

int make(int x,int y)
{
	if(y>0)
	{
		pattern(x-y+1);
		printf("\n");
		make(x,y-1);
	}
}

int main()
{
	int i,j;
	printf("Enter a number:");
	scanf("%d",&i);
	make(i,i);
	return 0;
}
